"use strict";

const {fail} = require("../utils/response-utils");
const {validationResult} = require("express-validator/check");

const validationHandler = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        let errMessage = "";
        for (let item of errors.array()) {
            errMessage += item.msg + "\n";
        }
        return res.json(fail(errMessage));
    } else {
        next();
    }
};

module.exports = {
    validationHandler
};
