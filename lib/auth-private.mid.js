"use strict";
const {unauthorized} = require("../utils/response-utils");

const authPrivate = (private_secret) => (req, res, next) => {
    const {secret} = req.headers;
    return secret === private_secret
        ? next()
        : res.json(unauthorized());
};

module.exports = {
    authPrivate
};
