"use strict";

const jwt = require("jsonwebtoken");
const {unauthorized} = require("../utils/response-utils");

const auth = (secret) => (req, res, next) => {
    const access_token = req.headers.authorization;
    jwt.verify(access_token, secret, function (err, decoded) {
        if (err) {
            const unauthorizedRes = unauthorized();
            return res.status(401).json(unauthorizedRes);
        } else {
            req.payload = decoded;
            req.payload.access_token = access_token;
            next();
        }
    });
};

module.exports = {
    auth
};
