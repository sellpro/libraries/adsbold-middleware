const {auth} = require("./auth.mid");
const {authAdmin} = require("./auth-admin.mid");
const {authPrivate} = require("./auth-private.mid");
const {validationHandler} = require("./validate.mid");

module.exports = {
    auth,
    authAdmin,
    authPrivate,
    validationHandler,
    validate: validationHandler
};
